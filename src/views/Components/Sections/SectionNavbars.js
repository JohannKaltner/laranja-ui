import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import DirectionsCarIcon from '@material-ui/icons/DirectionsCar';
import SearchIcon from '@material-ui/icons/Search'; 
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import PanToolIcon from '@material-ui/icons/PanTool';
 import PinDropIcon from '@material-ui/icons/PinDrop';
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import InfoArea from "components/InfoArea/InfoArea.js";

import styles from "assets/jss/material-kit-react/views/landingPageSections/productStyle.js";

const useStyles = makeStyles(styles);

export default function ProductSection() {
  const classes = useStyles();
  return (
    <div className={classes.section} style={{ backgroundColor: "#efefef" }}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={8}>
          <h2 className={classes.title} style={{ color: "#FF9800" }}>Como Funciona?</h2>
          <h5 className={classes.description}>
            Localize as melhores oficinas próximas de você e tenha tranquilidade e segurança para encontrar a
            melhor solução para o seu veículo
          </h5>
        </GridItem>
      </GridContainer>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="Preencha os campos com os detalhes do que você busca para o seu veículo"
              icon={DirectionsCarIcon}
              iconColor="warning"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="Encontre uma lista com as oficinas mais proximas da sua localização que oferecem o serviço desejado"
              icon={PinDropIcon}
              iconColor="warning"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="Avalie as oficinas listadas e feche negócio com aquela que se sentir mais confortavel"
              icon={SearchIcon}
              iconColor="warning"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="Caso haja qualquer divergência entre você e a oficína conte conosco para encontrar a melhor solução"
              icon={PanToolIcon}
              iconColor="warning"
              vertical
            />
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
